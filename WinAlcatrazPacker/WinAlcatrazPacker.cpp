// WinAlcatrazPacker.cpp : Defines the entry point for the application.
//

#include "framework.h"
#include "WinAlcatrazPacker.h"

#include "../PakReader/pak_utils.h"

#include <commdlg.h>
#include <ShlObj.h>
#include <shellapi.h>

#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;                                // current instance
WCHAR szTitle[MAX_LOADSTRING];                  // The title bar text
WCHAR szWindowClass[MAX_LOADSTRING];            // the main window class name

// Forward declarations of functions included in this code module:
ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

void startPacking(HWND hWnd);
void startUnpacking(HWND hWnd);
void unpack(HWND hWnd, LPCTSTR inputPath, LPCTSTR outputPath);
void pack(HWND hWnd, LPCTSTR inputPath, LPCTSTR outputPath);
bool parseCommandLine(LPCWSTR commandLine);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    if (parseCommandLine(lpCmdLine)) {
        return 0;
    }

    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // TODO: Place code here.

    // Initialize global strings
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_WINALCATRAZPACKER, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    // Perform application initialization:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_WINALCATRAZPACKER));

    MSG msg;

    // Main message loop:
    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_WINALCATRAZPACKER));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCEW(IDC_WINALCATRAZPACKER);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Store instance handle in our global variable

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, nullptr, nullptr, hInstance, nullptr);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE: Processes messages for the main window.
//
//  WM_COMMAND  - process the application menu
//  WM_PAINT    - Paint the main window
//  WM_DESTROY  - post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    switch (message)
    {
    case WM_COMMAND:
        {
            int wmId = LOWORD(wParam);
            // Parse the menu selections:
            switch (wmId)
            {
            case IDM_PACK:
                startPacking(hWnd);
                break;
            case IDM_UNPACK:
                startUnpacking(hWnd);
                break;
            case IDM_ABOUT:
                DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
                break;
            case IDM_EXIT:
                DestroyWindow(hWnd);
                break;
            default:
                return DefWindowProc(hWnd, message, wParam, lParam);
            }
        }
        break;
    case WM_PAINT:
        {
            PAINTSTRUCT ps;
            HDC hdc = BeginPaint(hWnd, &ps);
            // TODO: Add any drawing code that uses hdc here...
            EndPaint(hWnd, &ps);
        }
        break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));
            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}

std::unique_ptr<TCHAR[]> selectPakFile(HWND hWnd, bool save) {
    std::unique_ptr<TCHAR[]> filename{ new TCHAR[MAX_PATH]()};

    OPENFILENAME ofn = {
     .lStructSize = sizeof(OPENFILENAME),
     .hwndOwner = hWnd,
     .lpstrFilter = L"Pak\0*.pak\0All\0*.*\0",
     .nFilterIndex = 0,
     .lpstrFile = filename.get(),
     .nMaxFile = MAX_PATH,
     .Flags = save ? static_cast<DWORD>(0) : OFN_FILEMUSTEXIST,
    };

    if (save) {
        if (!GetSaveFileName(&ofn)) {
            return nullptr;
        }
    } else if (!GetOpenFileName(&ofn)) {
        return nullptr;
    }
    return filename;
}

// typedef int (CALLBACK* BFFCALLBACK)(HWND hwnd, UINT uMsg, LPARAM lParam, LPARAM lpData);

int FAR WINAPI browseNotify(HWND hWnd, UINT message, LPARAM lParam, LPARAM lpData) {
    if (message == BFFM_INITIALIZED) {
        static TCHAR currentDir[MAX_PATH];
        GetCurrentDirectory(MAX_PATH, currentDir);
        SendMessage(hWnd, BFFM_SETSELECTION, 1, reinterpret_cast<LPARAM>(currentDir));
        return 1;
    }
    return 0;
}

std::unique_ptr<TCHAR[]> selectDirectory(HWND hWnd) {
    std::unique_ptr<TCHAR[]> filename{ new TCHAR[MAX_PATH]()};

    TCHAR currentDirectory[MAX_PATH];
    GetCurrentDirectory(MAX_PATH, currentDirectory);
    BROWSEINFO bi = {
        .hwndOwner = hWnd,
        .lpszTitle = L"Select directory for unpacked files",
        .ulFlags = BIF_NEWDIALOGSTYLE,
        .lpfn = browseNotify
    };

    const auto item = SHBrowseForFolder(&bi);
    if (!item) {
        return nullptr;
    }
    SHGetPathFromIDList(item, filename.get());
    return filename;
}

void startPacking(HWND hWnd) {
    const auto dir = selectDirectory(hWnd);
    if (!dir) { return; }
    const auto pakFile = selectPakFile(hWnd, true);
    if (!pakFile) { return; }

    pack(hWnd, dir.get(), pakFile.get());
}

void startUnpacking(HWND hWnd) {
    const auto pakFile = selectPakFile(hWnd, false);
    if (!pakFile) { return; }
    const auto dir = selectDirectory(hWnd);
    if (!dir) { return; }

    unpack(hWnd, pakFile.get(), dir.get());
}

void unpack(HWND hWnd, LPCTSTR inputPath, LPCTSTR outputPath) {
    try {
        flow7f::unpack(inputPath, outputPath);
    }
    catch (std::exception& e) {
        MessageBoxA(hWnd, "Failed unpack", e.what(), MB_OK | MB_ICONERROR);
        return;
    }
    MessageBox(hWnd, L"Success!", L"Unpacked!", MB_OK);
}

void pack(HWND hWnd, LPCTSTR inputPath, LPCTSTR outputPath) {
    try {
        flow7f::pack(inputPath, outputPath);
    }
    catch (std::exception& e) {
        MessageBoxA(hWnd, "Failed pack", e.what(), MB_OK | MB_ICONERROR);
        return;
    }
    MessageBox(hWnd, L"Success!", L"Packed!", MB_OK);

}

bool parseCommandLine(LPCWSTR commandLine) {
    int numArgs = 0;
    const auto args = CommandLineToArgvW(commandLine, &numArgs);
    if (numArgs == 4) {
        if (args[1] == std::wstring(L"pack")) {
            pack(nullptr, args[2], args[3]);
            LocalFree(args);
            return true;
        } else if (args[1] == std::wstring(L"unpack")) {
            unpack(nullptr, args[2], args[3]);
            LocalFree(args);
            return true;
        }
    }

    LocalFree(args);
    return false;
}
