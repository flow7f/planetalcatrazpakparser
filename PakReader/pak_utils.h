#pragma once

#include <filesystem>

namespace flow7f {
    void unpack(const std::filesystem::path& inputPath, const std::filesystem::path& outPath);
    void pack(const std::filesystem::path& inputPath, const std::filesystem::path& outPath);
}
