#include <iostream>

#include "pak_utils.h"

void printUsage(const char* programPath) {
    const auto programName = std::filesystem::path{ programPath }.filename().string();
    std::cout << "Use: \n" <<
        "  " << programName << " [pack|unpack] input output\n" <<
        "for example: \n"
        "  " << programName << " pack data data.pak\n" <<
        "  " << programName << " unpack data.pack data" << std::endl;
}

int main(int argc, char** argv) {
    if (argc != 4) {
        printUsage(argv[0]);
    } else if (argv[1] == std::string("pack")) {
        flow7f::pack(argv[2], argv[3]);
    } else if (argv[1] == std::string("unpack")) {
        flow7f::unpack(argv[2], argv[3]);
    } else {
        std::cout << "Unknown command: " << argv[1] << std::endl;
        printUsage(argv[0]);
    }
    return 0;
}
