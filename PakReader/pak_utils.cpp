#include "pak_utils.h"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <memory>
#include <vector>

#include <random>

#include "zlib.h"

namespace {
    struct HeaderItem {
        char filename[128];
        uint32_t uncompressedSize;  // size after unpacking
        uint32_t compressedSize;    // size in pack (if !compressed, then equals uncompressedSize)
        uint32_t offset;            // offset + commpressedSize = next.offset (6 excl)
        uint32_t zeroFlag;          // always 0, probably offset should be uint64_t
        uint32_t e;                 // increase, except 52 excl, sometimes the same. Difference between values is multiplier with k=156253 (can be 0)
                                    //  min = 91480, max = 4294902523), max != UINT_MAX, but max + k cause uint64_t overflow.
                                    // Probably it's some kind of hash function. Tried to set it to 0 or as random value for every file and game work.
        uint32_t f;                 // 30015440 or 30015441 - tried both these values for every file, game works
        uint32_t compressed;        // zlib (uncompressedSize != compressedSize)
    };
    static_assert(sizeof(HeaderItem) == 156, "Incorrect HeaderItem size");

    std::unique_ptr<char[]> readItemContentFromPakFile(std::istream& istream, const HeaderItem& item) {
        std::unique_ptr<char[]> compressed(new char[item.compressedSize]);
        istream.seekg(item.offset, std::ios::beg);
        istream.read(compressed.get(), item.compressedSize);

        if (!item.compressed) {
            return compressed;
        }

        std::unique_ptr<char[]> data(new char[item.uncompressedSize]());
        uLongf longUncompressedSize = item.uncompressedSize;
        if (uncompress(
            reinterpret_cast<Bytef*>(data.get()),
            &longUncompressedSize,
            reinterpret_cast<const Bytef*>(compressed.get()),
            item.compressedSize) != Z_OK) {

            throw std::exception("Failed uncompress file");
        }

        return data;
    }

    std::vector<HeaderItem> readItems(std::istream& istream) {
        istream.seekg(0, std::ios::beg);
        uint32_t itemsCount;
        istream.read(reinterpret_cast<char*>(&itemsCount), sizeof(uint32_t));
        istream.seekg(4, std::ios::cur);

        std::vector<HeaderItem> items(itemsCount);
        istream.read(reinterpret_cast<char*>(items.data()), sizeof(HeaderItem) * itemsCount);
        return items;
    }
}

namespace flow7f {
    void unpack(const std::filesystem::path& inputPath, const std::filesystem::path& outPath) {
        std::ifstream input;
        input.exceptions(std::ifstream::failbit | std::ifstream::badbit);
        input.open(inputPath, std::ios::binary);

        const auto items = readItems(input);

        std::filesystem::create_directories(outPath);
        for (const auto& item : items) {
            const auto content = readItemContentFromPakFile(input, item);

            std::ofstream output;
            output.exceptions(std::ios::badbit | std::ios::failbit);
            output.open(outPath / item.filename, std::ios::binary | std::ios::trunc);
            output.write(content.get(), item.uncompressedSize);
        }
    }

    void pack(const std::filesystem::path& inputPath, const std::filesystem::path& outPath) {
        std::default_random_engine generator;
        std::uniform_int_distribution<uint32_t> distr;

        std::vector<HeaderItem> items;
        for (const auto& dir : std::filesystem::directory_iterator(inputPath)) {
            HeaderItem item;
            const auto filename = dir.path().filename().string();
            if (filename.size() >= sizeof(item.filename)) {
                std::cerr << "Too long filename: " << filename << std::endl;
                throw std::exception("Too long filename");
            }
            strcpy_s(item.filename, sizeof(item.filename), filename.c_str());
            item.uncompressedSize = static_cast<uint32_t>(dir.file_size());
            item.compressedSize = item.uncompressedSize;      // try not compress anything for
            item.offset = 0;
            item.zeroFlag = 0;
            item.e = distr(generator);
            item.f = 30015440;
            item.compressed = 0;
            items.emplace_back(item);
        }

        std::ofstream output;
        output.exceptions(std::ios::badbit | std::ios::failbit);
        output.open(outPath, std::ios::trunc | std::ios::binary);
        uint64_t size = items.size();
        output.write(reinterpret_cast<char*>(&size), sizeof(size));

        uint32_t offset = static_cast<uint32_t>(8 + items.size() * sizeof(HeaderItem));
        for (auto& item : items) {
            item.offset = offset;
            offset += item.compressedSize;
            output.write(reinterpret_cast<char*>(&item), sizeof(item));
        }
        for (const auto& item : items) {
            std::ifstream itemFile;
            itemFile.exceptions(std::ios::badbit | std::ios::failbit);
            itemFile.open(inputPath / item.filename, std::ios::binary);
            std::unique_ptr<char[]> data(new char[item.uncompressedSize]);
            itemFile.read(data.get(), item.uncompressedSize);
            output.write(data.get(), item.uncompressedSize);
        }
    }
}
